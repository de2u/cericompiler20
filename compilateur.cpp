//  A compiler from a very simple Pascal-like structured language LL(k)
//  to 64-bit 80x86 Assembly langage
//  Copyright (C) 2019 Pierre Jourlin
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Build with "make compilateur"


#include <string>
#include <iostream>
#include <cstdlib>
#include <set>
#include <map>
#include <FlexLexer.h>
#include "tokeniser.h"
#include <cstring>

using namespace std;

enum OPREL {EQU, DIFF, INF, SUP, INFE, SUPE, WTFR};
enum OPADD {ADD, SUB, OR, WTFA};
enum OPMUL {MUL, DIV, MOD, AND, WTFM};
enum TYPES {UNSIGNED_INT, BOOLEAN, DOUBLE, CHAR};

TOKEN current;				// Current token


FlexLexer* lexer = new yyFlexLexer; // This is the flex tokeniser
// tokens can be read using lexer->yylex()
// lexer->yylex() returns the type of the lexicon entry (see enum TOKEN in tokeniser.h)
// and lexer->YYText() returns the lexicon entry as a string


map<string, enum TYPES> DeclaredVariables;
unsigned long TagNumber=0;

bool IsDeclared(const char *id){
	return DeclaredVariables.find(id)!=DeclaredVariables.end();
}


void Error(string s){
	cerr << "Ligne n°"<<lexer->lineno()<<", lu : '"<<lexer->YYText()<<"'("<<current<<"), mais ";
	cerr<< s << endl;
	exit(-1);
}

// Program := VarDeclarationPart StatementPart
// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
// VarDeclaration := Letter {"," Letter} ":" Type
// StatementPart := Statement {";" Statement} "."

// Statement := AssignementStatement | DisplayAssignement | IfStatement | WhileStatement | ForStatement | BlockStatement | CaseStatement
// AssignementStatement := Letter ":=" Expression
// DisplayAssignement := "DISPLAY" Expression
// WhileStatement := "WHILE" Expression "DO" Statement
// IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]
// ForStatement := "FOR" AssignementStatement "TO" Expression "DO" Statement
// BlockStatement := "BEGIN" Statement {";" Statement} "END"
// CaseStatement := "CASE" Expression "OF" CaseListElement {";" CaseListElement} END
// CaseListElement := Expression ":" [Statement]

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
// SimpleExpression := Term {AdditiveOperator Term}
// Term := Factor {MultiplicativeOperator Factor}
// Factor := Number | Letter | "(" Expression ")"| "!" Factor
// Number := Digit{Digit}

// AdditiveOperator := "+" | "-" | "||"
// MultiplicativeOperator := "*" | "/" | "%" | "&&"
// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
// Digit := "0"|"1"|"2"|"3"|"4"|"5"|"6"|"7"|"8"|"9"
// Letter := "a"|...|"z"


TYPES Identifier(void){
	if (!IsDeclared(lexer->YYText())) {
		cerr << "Erreur : La variable '" << lexer->YYText() << "' n'est pas déclarée" << endl;
		exit(-1);
	}
	enum TYPES type = DeclaredVariables[lexer->YYText()];
	cout << "\tpush "<<lexer->YYText()<<endl;
	current=(TOKEN) lexer->yylex();
	return type;
}

TYPES Number(void){
	double d;
	unsigned int *i;
	string number = lexer->YYText();
	if (number.find(".")!=string::npos) {
		// nombre flottant
		d = atof(lexer->YYText());
		i = (unsigned int *) &d;	// i pointe sur la constante double

		cout << "\tsubq $8, %rsp \t\t\t# allocate 8 bytes on stack's top" << endl;
		cout << "\tmovl $" << *i << ", (%rsp)\t# conversion of " << d << " (32 bit high part)" << endl;
		cout << "\tmovl $" << *(i+1) << ", 4(%rsp)\t# conversion of " << d << " (32 bit low part)" << endl;
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	else {
		cout <<"\tpush $"<<atoi(lexer->YYText())<<endl;
		current=(TOKEN) lexer->yylex();
		return UNSIGNED_INT;
	}
}

TYPES CharConst(void) {
	cout << "\tmovq $0, %rax" << endl;
	cout << "\tmovb $" << lexer->YYText() << ", %al" << endl;
	cout << "\tpush %rax\t# push a 64-bit version of " << lexer->YYText() << endl;
	current=(TOKEN) lexer->yylex();
	return CHAR;
}

TYPES BoolConst(void) {
	if (strcmp(lexer->YYText(),"TRUE")==0)
		cout << "\tpush $0xFFFFFFFFFFFFFFFF\t# True" << endl;
	else if (strcmp(lexer->YYText(),"FALSE")==0)
		cout << "\tpush $0\t\t\t# False" << endl;
	current=(TOKEN) lexer->yylex();
	return BOOLEAN;
}

TYPES Expression(void);			// Called by Term() and calls Term()

TYPES Factor(void){
	enum TYPES type;
	switch (current) {
		case RPARENT:
			current=(TOKEN) lexer->yylex();
			type=Expression();
			if(current!=LPARENT)
				Error("')' était attendu");		// ")" expected
			else
				current=(TOKEN) lexer->yylex();
			break;
		case NUMBER:
			type=Number();
			break;
    case ID:
			type=Identifier();
			break;
		case CHARCONST:
			type=CharConst();
			break;
		case KEYWORD:
			if (strcmp(lexer->YYText(),"TRUE")==0 || strcmp(lexer->YYText(),"FALSE")==0)
				type=BoolConst();
			break;
		default:
			Error("'(' ou variable ou constante attendue");
	}
	return type;
}

// MultiplicativeOperator := "*" | "/" | "%" | "&&"
OPMUL MultiplicativeOperator(void){
	OPMUL opmul;
	if(strcmp(lexer->YYText(),"*")==0)
		opmul=MUL;
	else if(strcmp(lexer->YYText(),"/")==0)
		opmul=DIV;
	else if(strcmp(lexer->YYText(),"%")==0)
		opmul=MOD;
	else if(strcmp(lexer->YYText(),"&&")==0)
		opmul=AND;
	else opmul=WTFM;
	current=(TOKEN) lexer->yylex();
	return opmul;
}

// Term := Factor {MultiplicativeOperator Factor}
TYPES Term(void){
	OPMUL mulop;
	enum TYPES type1, type2;
	type1=Factor();
	while(current==MULOP){
		mulop=MultiplicativeOperator();		// Save operator in local variable
		type2=Factor();
		if (type1 != type2)
			Error("Different Factor types");
		switch(mulop){
			case AND:
				if (type2 != BOOLEAN)
					Error("type non BOOLEAN pour l'opérateur AND");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
				cout << "\tpush %rax\t# AND"<<endl;	// store result
				break;
			case MUL:
				if (type2 != UNSIGNED_INT && type2 != DOUBLE)
					Error("type non UNSIGNED_INT ou DOUBLE pour la multiplication");
				if (type2 == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmulq	%rbx"<<endl;	// a * b -> %rdx:%rax
					cout << "\tpush %rax\t# MUL"<<endl;	// store result
				}
				else {
					cout << "\tfldl 8(%rsp)\t" << endl;
					cout << "\tfldl (%rsp)\t# first operand -> %st(0); second operand -> %st(1)" << endl;
					cout << "\tmulp %st(0), %st(1)\t# %st(0) <- op1 + op2; %st(1)=null" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp\t# result on stack's top" << endl;
				}
				break;
			case DIV:
				if (type2 != UNSIGNED_INT && type2 != DOUBLE)
					Error("type non UNSIGNED_INT ou DOUBLE pour la division");
				if (type2 == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator
					cout << "\tdiv %rbx"<<endl;			// quotient goes to %rax
					cout << "\tpush %rax\t# DIV"<<endl;		// store result
				}
				else {
					cout << "\tfldl (%rsp)\t" << endl;
					cout << "\tfldl 8(%rsp)\t# first operand -> %st(0); second operand -> %st(1)" << endl;
					cout << "\tfdivp %st(0), %st(1)\t# %st(0) <- op1 + op2; %st(1)=null" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp\t# result on stack's top" << endl;
				}
				break;
			case MOD:
				if (type2 != UNSIGNED_INT)
					Error("type non UNSIGNED_INT pour le modulo");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\tmovq $0, %rdx"<<endl; 	// Higher part of numerator
				cout << "\tdiv %rbx"<<endl;			// remainder goes to %rdx
				cout << "\tpush %rdx\t# MOD"<<endl;		// store result
				break;
			default:
				Error("opérateur multiplicatif attendu");
		}
	}
	return type1;
}

// AdditiveOperator := "+" | "-" | "||"
OPADD AdditiveOperator(void){
	OPADD opadd;
	if(strcmp(lexer->YYText(),"+")==0)
		opadd=ADD;
	else if(strcmp(lexer->YYText(),"-")==0)
		opadd=SUB;
	else if(strcmp(lexer->YYText(),"||")==0)
		opadd=OR;
	else opadd=WTFA;
	current=(TOKEN) lexer->yylex();
	return opadd;
}

// RelationalOperator := "==" | "!=" | "<" | ">" | "<=" | ">="
OPREL RelationalOperator(void){
	OPREL oprel;
	if(strcmp(lexer->YYText(),"==")==0)
		oprel=EQU;
	else if(strcmp(lexer->YYText(),"!=")==0)
		oprel=DIFF;
	else if(strcmp(lexer->YYText(),"<")==0)
		oprel=INF;
	else if(strcmp(lexer->YYText(),">")==0)
		oprel=SUP;
	else if(strcmp(lexer->YYText(),"<=")==0)
		oprel=INFE;
	else if(strcmp(lexer->YYText(),">=")==0)
		oprel=SUPE;
	else oprel=WTFR;
	current=(TOKEN) lexer->yylex();
	return oprel;
}

// SimpleExpression := Term {AdditiveOperator Term}
TYPES SimpleExpression(void){
	OPADD adop;
	enum TYPES type1, type2;
	type1=Term();
	while(current==ADDOP){
		adop=AdditiveOperator();		// Save operator in local variable
		type2=Term();
		if (type1!=type2)
			Error("Different Term types");
		switch(adop){
			case OR:
				if (type2 != BOOLEAN)
					Error("type non BOOLEAN pour l'opérateur OR");
				cout << "\tpop %rbx"<<endl;	// get first operand
				cout << "\tpop %rax"<<endl;	// get second operand
				cout << "\torq	%rbx, %rax\t# OR"<<endl;// operand1 OR operand2
				cout << "\tpush %rax" << endl;	// store result
				break;
			case ADD:
				if (type2 != UNSIGNED_INT && type2 != DOUBLE)
					Error("type non UNSIGNED_INT ou DOUBLE pour l'addition");
				if (type2 == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\taddq	%rbx, %rax\t# ADD"<<endl;	// add both operands
					cout << "\tpush %rax" << endl;	// store result
				}
				else {
					cout << "\tfldl 8(%rsp)\t" << endl;
					cout << "\tfldl (%rsp)\t# first operand -> %st(0); second operand -> %st(1)" << endl;
					cout << "\tfaddp %st(0), %st(1)\t# %st(0) <- op1 + op2; %st(1)=null" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp\t# result on stack's top" << endl;
				}
				break;
			case SUB:
				if (type2 != UNSIGNED_INT && type2 != DOUBLE)
					Error("type non UNSIGNED_INT ou DOUBLE pour la soustraction");
				if (type2 == UNSIGNED_INT) {
					cout << "\tpop %rbx"<<endl;	// get first operand
					cout << "\tpop %rax"<<endl;	// get second operand
					cout << "\tsubq	%rbx, %rax\t# SUB"<<endl;	// substract both operands
					cout << "\tpush %rax" << endl;	// store result
				}
				else {
					cout << "\tfldl (%rsp)\t" << endl;
					cout << "\tfldl 8(%rsp)\t# first operand -> %st(0); second operand -> %st(1)" << endl;
					cout << "\tfsubp %st(0), %st(1)\t# %st(0) <- op1 + op2; %st(1)=null" << endl;
					cout << "\tfstpl 8(%rsp)" << endl;
					cout << "\taddq $8, %rsp\t# result on stack's top" << endl;
				}
				break;
			default:
				Error("opérateur additif inconnu");
		}
	}
	return type1;
}

// Expression := SimpleExpression [RelationalOperator SimpleExpression]
TYPES Expression(void){
	OPREL oprel;
	enum TYPES type1, type2;
	type1=SimpleExpression();
	if(current==RELOP){
		oprel=RelationalOperator();
		type2=SimpleExpression();
		if (type1!=type2)
			Error("Different SimpleExpression types");
		cout << "\tpop %rax"<<endl;
		cout << "\tpop %rbx"<<endl;
		cout << "\tcmpq %rax, %rbx"<<endl;
		switch(oprel){
			case EQU:
				cout << "\tje Vrai"<<++TagNumber<<"\t# If equal"<<endl;
				break;
			case DIFF:
				cout << "\tjne Vrai"<<++TagNumber<<"\t# If different"<<endl;
				break;
			case SUPE:
				cout << "\tjae Vrai"<<++TagNumber<<"\t# If above or equal"<<endl;
				break;
			case INFE:
				cout << "\tjbe Vrai"<<++TagNumber<<"\t# If below or equal"<<endl;
				break;
			case INF:
				cout << "\tjb Vrai"<<++TagNumber<<"\t# If below"<<endl;
				break;
			case SUP:
				cout << "\tja Vrai"<<++TagNumber<<"\t# If above"<<endl;
				break;
			default:
				Error("Opérateur de comparaison inconnu");
		}
		cout << "\tpush $0\t\t# False"<<endl;
		cout << "\tjmp Suite"<<TagNumber<<endl;
		cout << "Vrai"<<TagNumber<<":\tpush $0xFFFFFFFFFFFFFFFF\t\t# True"<<endl;
		cout << "Suite"<<TagNumber<<":"<<endl;
		return BOOLEAN;	// because relop
	}
	return type1;
}

void Statement(void);

// AssignementStatement := Identifier ":=" Expression
void AssignementStatement(void){
	string variable;
	enum TYPES type1, type2;
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1 = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if (type1!=type2) {
		cerr << "Variable type " << type1 << endl;
		cerr << "Expression type " << type2 << endl;
		Error("Different Assignement types");
	}
	cout << "\tpop " << variable << endl;
}

// DisplayAssignement := "DISPLAY" Expression
void DisplayAssignement(void) {
	enum TYPES type;
	unsigned long long buffer = ++TagNumber;
	if (current==KEYWORD && strcmp(lexer->YYText(),"DISPLAY")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé DISPLAY attendu");
	type=Expression();
	switch (type) {
		case UNSIGNED_INT:
			cout << "\tpop %rdx\t# The value to be displayed" << endl;
			cout << "\tmovq $FormatString1, %rsi\t# \"%llu\\n\"" << endl;
			cout << "\tmovl $1, %edi" << endl;
			cout << "\tmovl $0, %eax" << endl;
			cout << "\tcall __printf_chk@PLT" << endl;
			break;
		case BOOLEAN:
			cout << "\tpop %rdx\t# zero: false, non-zero: true" << endl;
			cout << "\tcmpq $0, %rdx" << endl;
			cout << "\tje False" << buffer << endl;
			cout << "\tmovq $TrueString, %rdi\t# \"TRUE\\n\"" << endl;
			cout << "\tjmp Next" << buffer << endl;
			cout << "False" << buffer << ":" << endl;
			cout << "\tmovq $FalseString, %rdi\t# \"FALSE\\n\"" << endl;
			cout << "Next" << buffer << ":" << endl;
			cout << "\tcall puts@PLT" << endl;
			break;
		case DOUBLE:
			cout << "\tmovsd (%rsp), %xmm0\t\t# &stack top -> %xmm0" << endl;
			cout << "\tsubq $16, %rsp\t\t# allocation for 3 additional doubles" << endl;
			cout << "\tmovsd %xmm0, 8(%rsp)" << endl;
			cout << "\tmovq $FormatString2, %rdi\t# \"%lf\\n\"" << endl;
			cout << "\tmovq $1, %rax" << endl;
			cout << "\tcall printf" << endl;
			cout << "nop" << endl;
			cout << "\taddq $24, %rsp\t\t# pop nothing" << endl;
			break;
		case CHAR:
			cout << "\tpop %rsi\t\t\t# get character in the 8 lowest bits of %si" << endl;
			cout << "\tmovq $FormatString3, %rdi\t# \"%c\\n\"" << endl;
			cout << "\tmovl $0, %eax" << endl;
			cout << "\tcall printf@PLT" << endl;
			break;
		default:
			Error("DISPLAY: type de donnée non supporté");
	}
}

// IfStatement := "IF" Expression "THEN" Statement ["ELSE" Statement]
void IfStatement(void) {
	unsigned long long buffer = ++TagNumber;
	enum TYPES type;
	if (current==KEYWORD && strcmp(lexer->YYText(),"IF")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé IF attendu");
	cout << "If" << buffer << ":" << endl;
	type=Expression();
	if (type!=BOOLEAN)
		Error("Expression dans If pas de type BOOLEAN");
	if (current==KEYWORD && strcmp(lexer->YYText(),"THEN")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé THEN attendu");
	cout << "\tpop %rax" << endl;
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tje Else" << buffer << "\t# False" << endl;
	Statement();
	cout << "\tjmp FinIf" << buffer << endl;
	cout << "Else" << buffer << ":" << endl;
	if (current==KEYWORD && strcmp(lexer->YYText(),"ELSE")==0) {
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	cout << "FinIf" << buffer << ":" << endl;
}

// WhileStatement := "WHILE" Expression "DO" Statement
void WhileStatement(void) {
	unsigned long long buffer = ++TagNumber;
	enum TYPES type;
	if (current==KEYWORD && strcmp(lexer->YYText(),"WHILE")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé WHILE attendu");
	cout << "While" << buffer << ":" << endl;
	type=Expression();
	if (type!=BOOLEAN)
		Error("Expression dans While pas de type BOOLEAN");
	if(current==KEYWORD && strcmp(lexer->YYText(),"DO")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé Do attendu");
	cout << "\tpop %rax" << endl;
	cout << "\tcmpq $0, %rax" << endl;
	cout << "\tje FinWhile" << buffer << "\t# False" << endl;
	Statement();
	cout << "\tjmp While" << buffer << endl;
	cout << "FinWhile" << buffer << ":" << endl;
}

// ForStatement := "FOR" Identifier ":=" Expression "TO" Expression "DO" Statement
void ForStatement(void) {
	unsigned long long buffer = ++TagNumber;
	string variable;
	enum TYPES type1, type2, type3;
	if (current==KEYWORD && strcmp(lexer->YYText(),"FOR")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé FOR attendu");
	cout << "For" << buffer << ":" << endl;
	// AssignementStatement
	if(current!=ID)
		Error("Identificateur attendu");
	if(!IsDeclared(lexer->YYText())){
		cerr << "Erreur : Variable '"<<lexer->YYText()<<"' non déclarée"<<endl;
		exit(-1);
	}
	variable=lexer->YYText();
	type1 = DeclaredVariables[variable];
	current=(TOKEN) lexer->yylex();
	if(current!=ASSIGN)
		Error("caractères ':=' attendus");
	current=(TOKEN) lexer->yylex();
	type2=Expression();
	if (type1!=type2) {
		cerr << "Variable type " << type1 << endl;
		cerr << "Expression type " << type2 << endl;
		Error("Different Assignement types in FOR");
	}
	cout << "\tpop " << variable << endl;

	if (current==KEYWORD && strcmp(lexer->YYText(),"TO")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé TO attendu");
	cout << "ForLoop" << buffer << ":" << endl;
	type3=Expression();
	if (type2!=type3)
		Error("types de variables différents dans FOR");
	if (current==KEYWORD && strcmp(lexer->YYText(),"DO")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé DO attendu");
	cout << "\tpop %rax" << endl;
	cout << "\tcmpq " << variable << ", %rax" << endl;
	cout << "\tje FinFor" << buffer << "\t# False" << endl;
	// Letting the user increment/decrement himself
	Statement();
	cout << "\tjmp ForLoop" << buffer << endl;
	cout << "FinFor" << buffer << ":" << endl;
}

// BlockStatement := "BEGIN" Statement {";" Statement} "END"
void BlockStatement(void) {
	unsigned long long buffer = ++TagNumber;
	if (current==KEYWORD && strcmp(lexer->YYText(),"BEGIN")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé BEGIN attendu");
	cout << "Block" << buffer << ":" << endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if (current==KEYWORD && strcmp(lexer->YYText(),"END")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé END attendu");
}

// CaseListElement := Expression ":" [Statement] ["BREAK"]
void CaseListElement(TYPES type1, unsigned long long casebuffer, unsigned long long listbuffer) {
	enum TYPES type2;
	cout << "Case" << casebuffer << "ListElement" << listbuffer << ":" << endl;
	type2=Expression();
	if (type1!=type2)
		Error("Differents types d'expression dans CASE");
	if (current!=COLON)
		Error("':' manquant dans CaseListElement");
	current=(TOKEN) lexer->yylex();
	cout << "\tpop %rax"<<endl;
	cout << "\tpop %rbx"<<endl;
	cout << "\tcmpq %rax, %rbx"<<endl;
	cout << "\tjne FinCase" << casebuffer << "ListElement" << listbuffer << "\t# False" << endl;
	cout << "Case" << casebuffer << "ListElement" << listbuffer << "Statement:" << endl;
	if (current!=SEMICOLON)
		Statement();
		if (current==KEYWORD && strcmp(lexer->YYText(),"BREAK")==0) {
			current=(TOKEN) lexer->yylex();
			cout << "\tjmp FinCase" << casebuffer << endl;
		}
		if (current==SEMICOLON)
			cout << "\tjmp Case" << casebuffer << "ListElement" << listbuffer+1 << "Statement" << endl;
	cout << "FinCase" << casebuffer << "ListElement" << listbuffer << ":" << endl;
}

// CaseStatement := "CASE" Expression "OF" CaseListElement {";" CaseListElement} END
void CaseStatement(void) {
	unsigned long long buffer = ++TagNumber;
	unsigned long long listbuffer = 0;
	enum TYPES type;
	if (current==KEYWORD && strcmp(lexer->YYText(),"CASE")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé CASE attendu");
	cout << "Case" << buffer << ":" << endl;
	type=Expression();
	cout << "\tpop %rcx\t\t# save value of case expression" << endl;
	cout << "\tpush %rcx\t\t# and put it back" << endl;
	if (current==KEYWORD && strcmp(lexer->YYText(),"OF")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé OF attendu");
	CaseListElement(type, buffer, ++listbuffer);
	while (current==SEMICOLON) {
		current=(TOKEN) lexer->yylex();
		cout << "\tpush %rcx\t\t# put value of first expression back on the stack's top" << endl;
		CaseListElement(type, buffer, ++listbuffer);
	}
	if (current==KEYWORD && strcmp(lexer->YYText(),"END")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé END attendu");
	cout << "FinCase" << buffer << ":" << endl;
}

// Statement := AssignementStatement | DisplayAssignement | IfStatement | WhileStatement | ForStatement | BlockStatement
void Statement(void){
	if (current==KEYWORD) {
		if (strcmp(lexer->YYText(),"WHILE")==0)
			WhileStatement();
		else if (strcmp(lexer->YYText(),"IF")==0)
			IfStatement();
		else if (strcmp(lexer->YYText(),"FOR")==0)
			ForStatement();
		else if (strcmp(lexer->YYText(),"BEGIN")==0)
			BlockStatement();
		else if (strcmp(lexer->YYText(),"DISPLAY")==0)
			DisplayAssignement();
		else if (strcmp(lexer->YYText(),"CASE")==0)
			CaseStatement();
		else
			Error("Mot clé attendu");
	}
	else if (current==ID)
		AssignementStatement();
	else if (current==SEMICOLON);	// empty statement
	else
		Error("Statement attendu");
}

// StatementPart := Statement {";" Statement} "."
void StatementPart(void){
	cout << "\t.text\t\t# The following lines contain the program"<<endl;
	cout << "\t.globl main\t# The main function must be visible from outside"<<endl;
	cout << "main:\t\t\t# The main function body :"<<endl;
	cout << "\tmovq %rsp, %rbp\t# Save the position of the stack's top"<<endl;
	Statement();
	while(current==SEMICOLON){
		current=(TOKEN) lexer->yylex();
		Statement();
	}
	if(current!=DOT)
		Error("caractère '.' attendu");
	current=(TOKEN) lexer->yylex();
}

TYPES Type(void) {
	if (current!=KEYWORD)
		Error("Type attendu");
	if (strcmp(lexer->YYText(),"UNSIGNED_INT")==0) {
		current=(TOKEN) lexer->yylex();
		return UNSIGNED_INT;
	}
	if (strcmp(lexer->YYText(),"BOOLEAN")==0) {
		current=(TOKEN) lexer->yylex();
		return BOOLEAN;
	}
	if (strcmp(lexer->YYText(),"DOUBLE")==0) {
		current=(TOKEN) lexer->yylex();
		return DOUBLE;
	}
	if (strcmp(lexer->YYText(),"CHAR")==0) {
		current=(TOKEN) lexer->yylex();
		return CHAR;
	}
	else
		Error("Type inconnu");
}

// VarDeclaration := Identifier {"," Identifier} ":" Type
void VarDeclaration(void) {
	set<string> VarToDeclare;
	enum TYPES type;
	if (current!=ID)
		Error("Id attendu");
	VarToDeclare.insert(lexer->YYText());
	current=(TOKEN) lexer->yylex();
	while (current==COMMA) {
		current=(TOKEN) lexer->yylex();
		if (current!=ID)
			Error("Id attendu");
		VarToDeclare.insert(lexer->YYText());
		current=(TOKEN) lexer->yylex();
	}
	if (current!=COLON)
		Error("Type de variable manquant");
	current=(TOKEN) lexer->yylex();
	type = Type();
	for (set<string>::iterator i=VarToDeclare.begin(); i!=VarToDeclare.end(); ++i) {
		switch (type) {
			case BOOLEAN:
			case UNSIGNED_INT:
				cout << *i << ":\t.quad 0" << endl;
				break;
			case DOUBLE:
				cout << *i << ":\t.double 0.0" << endl;
				break;
			case CHAR:
				cout << *i << ":\t.byte 0" << endl;
				break;
			default:
				Error("type inconnu");
		};
		DeclaredVariables[*i]=type;
	}
}

// VarDeclarationPart := "VAR" VarDeclaration {";" VarDeclaration} "."
void VarDeclarationPart(void) {
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0)
		current=(TOKEN) lexer->yylex();
	else
		Error("Mot clé VAR attendu");
	VarDeclaration();
	while (current==SEMICOLON) {
		current=(TOKEN) lexer->yylex();
		VarDeclaration();
	}
	if (current!=DOT)
		Error("caractère '.' attendu à la fin des déclarations");
	current=(TOKEN) lexer->yylex();
}

// Program := VarDeclarationPart StatementPart
void Program(void){
	if(current==KEYWORD && strcmp(lexer->YYText(),"VAR")==0)
		VarDeclarationPart();
	StatementPart();
}

int main(void){	// First version : Source code on standard input and assembly code on standard output
	// Header for gcc assembler / linker
	cout << "\t\t\t# This code was produced by the CERI Compiler"<<endl;
	cout << "\t.data"<<endl;
	cout << "FormatString1:\t.string \"%llu\"\t# used by printf to display 64-bit unsigned integers" << endl;
	cout << "FormatString2:\t.string \"%lf\"\t# used by printf to display 64-bit floating point numbers" << endl;
	cout << "FormatString3:\t.string \"%c\"\t# used by printf to display a 8-bit single character" << endl;
	cout << "TrueString:\t.string \"TRUE\"\t# used by printf to display the boolean value TRUE" << endl;
	cout << "FalseString:\t.string \"FALSE\"\t# used by printf to display the boolean value FALSE" << endl;
	// Let's proceed to the analysis and code production
	current=(TOKEN) lexer->yylex();
	Program();
	// Trailer for the gcc assembler / linker
	cout << "\tmovq %rbp, %rsp\t\t# Restore the position of the stack's top"<<endl;
	cout << "\tret\t\t\t# Return from main function"<<endl;
	if(current!=FEOF){
		cerr <<"Caractères en trop à la fin du programme : ["<<current<<"]";
		Error("."); // unexpected characters at the end of program
	}

}
